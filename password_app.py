import random


class PasswordApp:

    def __init__(self, myfile):
        self.words = myfile.read().split("\n")[-5000:]
        random.shuffle(self.words)

    def generate(self, n_words=5):
        return ' '.join([self.words[random.randrange(len(self.words))] for i in range(n_words)])
