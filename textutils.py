
def render_txt2html(s):
    return s.replace('\n', '<br>')


def prepare_textfile_body(s, analysis_type="letter"):
    """Prepare a string of body text for feeding into a pymarkoff.Markov object.
    Analysis type is required.
    """
    seeds = []
    if analysis_type == "word":
        # split by newlines then by spaces
        seeds = [i.strip().split(' ')
                 for i in s.split('\n') if len(i.strip()) > 0]
    elif analysis_type == "letter":
        # split by newlines then by characters
        seeds = [i.strip() for i in s.strip().split('\n') if len(i.strip()) > 0]
        seeds = [list(i) for i in seeds]
    else:
        raise ValueError("Invalid analysis type:{}".format(analysis_type))

    return seeds
