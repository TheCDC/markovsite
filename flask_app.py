#!/usr/bin/env python3
# A very simple Flask Hello World app for you to get started with...
import flask
from flask import Flask
from flask import request
from flask import url_for
from flask import render_template
import os
import pymarkoff
import textutils
from markov_helpers import MarkovFile
from password_app import PasswordApp
import random
app = Flask(__name__)

app.config["tmp"] = "/tmp"

# DEFAULT_CSS = url_for('static', filename='style.css')

password_generator = None
sources = []
all_contents = []
brains = {}
name_to_contents = {}


@app.route("/tmp/<path:filename>")
def serve_temp_file(filename):
    return flask.send_from_directory("/tmp", filename)


def listfullpaths(p):
    return [os.path.join(p, i) for i in os.listdir(p)]


def testMarkovFile():
    some_sources = sorted(listfullpaths("markov_corpora/"))
    for i in some_sources:
        b = MarkovFile(i)
        b.generate()


@app.route("/")
def hello_world():
    """WOOT"""
    return render_template(
        "index.html",
        title="Christopher Chen",
        css=url_for('static', filename='style.css'))


@app.route("/markov/_reload")
def request_reload():
    reload_caches()
    return "Reloaded cached files and content generators!"


def reload_caches():
    global sources
    global all_contents
    global brains
    global password_generator
    sources = []
    all_contents = []
    brains = {}
    sources = sorted(
        listfullpaths(
            os.path.join(os.path.dirname(__file__), "markov_corpora")))
    for s in sources:
        # with open(s) as f:
        #     all_contents.append(f.read())
        for t in ["word", "letter"]:
            b = MarkovFile(s, (0, 7), t)
            brains.update({b.name: b})

    # for s, c in zip(sources, all_contents):
    #     seeds = [[''] + list(i) + ['.'] for i in c.split('\n')]
    #     brains.update({s: pymarkoff.Markov(seeds, (0, 7))})
    password_generator = PasswordApp(
        open("sources/google-10000-english-no-swears.txt"))
    return (sources, all_contents)


@app.before_first_request
def setup():
    reload_caches()


def markov_link_from_filename(name, text=None):
    if text:
        return "<a href=\"/markov/preloaded/{0}\">{1}</a>".format(name, text)
    else:
        return "<a href=\"/markov/preloaded/{0}\">{0}</a>".format(name)


def prep_section(name, contents):
    return '<h2 class="markov_source_title" >{0}</h2><p>{1}</p><p>{2}</p>'.format(
        name, markov_link_from_filename(name, "Refresh"),
        '<br>'.join(contents))


def gen_and_filter(m, *, num_chains=30, min_len=1, delimiter=''):
    l = [m.generate(max_length=50) for i in range(num_chains)]
    d = delimiter
    try:
        d = m.delimiter
    except AttributeError:
        pass
    l = [d.join(i) for i in l if len(i) >= min_len]
    l = [i for i in l if len(i) >= min_len]
    return l


@app.route("/markov/submit")
def custom_chain_submit():
    return render_template(
        "markov_submit.html", css=url_for('static', filename='style.css'))


@app.route("/markov/preloaded/<path:filename>", methods=["GET"])
def fetch_and_chain(filename):
    # return filename
    try:
        return render_template(
            "markov_output.html",
            body=prep_section(filename,
                              gen_and_filter(brains[filename], num_chains=30)),
            css=url_for('static', filename='style.css'))
    except KeyError:
        return "Bad file!"


@app.route("/markov/output", methods=["GET", "POST"])
def custom_markov_output():
    if request.method == "POST":
        # assert request.path == "/markov"
        # return "POST"
        max_chains = int(request.form["max_chains"])
        min_len = int(request.form["min_chain_length"])
        analysis_type = request.form["analysis_type"]
        # return(repr(request.form))
        try:
            distances = tuple(
                [int(i) for i in request.form["distances"].split(" ")])
        except:
            distances = (0, )

        delimiter = ''
        if analysis_type == "word":
            delimiter = ' '
        else:
            delimiter = ''

        seeds = textutils.prepare_textfile_body(
            request.form["chains"], analysis_type=analysis_type)

        b = pymarkoff.Markov(seeds, distances)
        results = []
        for _ in range(max_chains):
            if analysis_type == "word":
                results.append(b.next_sentence())
            # case of analysis by letters
            else:
                results.append(b.next_word())
        # results = gen_and_filter(
        #     b, num_chains=max_chains, delimiter=delimiter, min_len=min_len)

        # results = [i for i in results if len(i.strip()) > 0]
        # + "<p>" + str(dict(b)) + "</p>"
        db = dict(b)
        nt = sum(map(len, db.values()))
        info_card = render_template(
            "custom_markov_stats.html", num_states=len(db), num_transitions=nt)
        temp_location = None
        # if nt <= 50:
        #     temp_location = cdn_utils.get_tempname(suffix=".png")
        #     graph = b.to_graph()
        #     graph.write_png(os.path.join("/tmp", temp_location))
        return render_template(
            'markov_output.html',
            body='<br>'.join(results),
            css=url_for('static', filename='style.css'),
            # graph_img=temp_location,
            info_table=info_card)
    else:
        return render_template(
            'markov_output.html',
            body=
            'Woops! It looks like your data timed out. <a href="/markov/submit">Care to try again</a>?',
            css=url_for('static', filename='style.css'))


@app.route("/markov")
def markov_pages():
    """Serve the markov pages"""
    sections = []
    sections = sorted([
        markov_link_from_filename(b.name, text=b.name)
        for b in brains.values()
    ])

    sections_html = "<h2>Choose a source for random text generation</h2>" + '<a href="/markov/submit"><h3>Or make your own!</h3></a>' + "<hr><ul><li>" + \
        '</li><li>'.join(sections) + "</li></ul>"
    return render_template(
        "blank.html",
        body=sections_html,
        title="Markov Chain Generator",
        css=url_for('static', filename='style.css'))


@app.errorhandler(404)
def page_not_found(e):
    return render_template(
        '404.html', css=url_for('static', filename='style.css')), 404


@app.route("/passwords")
def page_strong_passwords():
    some_passwords = [
        password_generator.generate(random.randint(3, 7)) for i in range(10)
    ]
    header = "<h2 class=\"markov_source_title\" >Memorable Passwords, inspiration: <a href=\"https://xkcd.com/936/\">xkcd</a></h2><h3>Refresh for new passwords</h3>"
    return render_template(
        "generated_passwords.html",
        body=header + "<br>".join(some_passwords),
        title="Strong and memorable passwords",
        css=url_for('static', filename='style.css'))


@app.route("/js/<path:filename>")
def serve_js(filename):
    # return os.getcwd()
    return url_for("static", filename=os.path.join("js", filename))


@app.route("/css/<path:filename>")
def serve_css(filename):
    # return os.getcwd()
    return url_for("static", filename=os.path.join("css", filename))


# ========== Critter Competition Controller
critter_controller_state = {
    'move': 0,
}


@app.route('/critter', methods=["GET", "POST"])
def critter_controller():
    if request.method == 'POST':
        critter_controller_state.update({
            'move': int(request.form.get('move'))
        })
        return flask.redirect(flask.url_for('critter_viewer'))
    else:
        return flask.jsonify(critter_controller_state)


@app.route('/critter/view')
def critter_viewer():
    context = dict(moves=range(10), state=critter_controller_state)
    return flask.render_template('critter_view.html', context=context)
