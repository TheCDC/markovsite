import tempfile
from flask import url_for


def get_tempname(suffix=""):
    with tempfile.NamedTemporaryFile(suffix=suffix) as f:
        # f.write("")
        # print(f.name)
        print(f.name)
        return f.name


def main():
    get_tempname()

if __name__ == '__main__':
    main()
