#!/usr/bin/env python3
import os
import textutils
import pymarkoff
class MarkovFile():
    """Associate a file and its Markov generator brain.
    Also contains many of the options used for the Markov brain in the first place.
    Used to remember what delimiters to use for I/O."""
    analysis_type_to_delimiter = {"word": ' ', "letter": ''}

    def __init__(self, filepath, distances=(0,), analysis_type="word"):
        self.path = filepath
        self.filename = os.path.basename(filepath)
        self.analysis_type = analysis_type
        if analysis_type == "word":
            self.delimiter = ' '
        elif analysis_type == "letter":
            self.delimiter = ''
        else:
            raise ValueError("Invalid analysis type!")
        self.distances = distances
        self.brain = None
        self._loadfile()
        self.generate = self.brain.generate
        self.name = self.path + '-' + self.analysis_type

    def _loadfile(self):
        with open(self.path, encoding="utf8") as f:
            contents = f.read()

        seeds = textutils.prepare_textfile_body(
            contents, analysis_type=self.analysis_type)

        self.brain = pymarkoff.Markov(seeds, self.distances)

    # def generate(self, *args, **kwargs):
    #     return self.brain.generate(*args, **kwargs)

    def __str__(self):
        return "fname={},atype={},dists={}".format(self.path, self.analysis_type, self.distances)

